# Symphony Tools

#### Collection of modules to manage company functions such as per diem, leave, and trip paperwork.

## Sprint 1: Leave
### User Stories:

+ Employee able to login and logout using company email. Password reset function availible. (complete)
+ Employee able to make a leave request. (complete)
+ Employee able to check the status of a leave request. (complete)
+ Employee notified by email when leave request is approved.
+ Employee able to check amount of leave remaining. (complete)
+ Admin able to see all leave requests, and sort by status, data, employee.
+ Admin able to approve or deny leave requests.
+ Admin able to generate leave report as PDF.
+ Admin able to create new employees, and manage employees created already (complete)

### Deliverables:
+ login / logout / password reset
+ user management system including role access management and security
+ leave management system including emailing and reporting
+ backend systems Laravel (email, PDF reporting)

## Technical Details:
+ Front End Framework - Angular JS 1.4.x
+ Datebase - Firebase
+ Backend - Laravel

To test this project:
+ clone locally
+ ensure node, bower, and gulp are installed
+ install dependancies using bower
+ create Firebase database (free tier availbile)
+ edit src/app/components/common/constants-example.js with firebase URL and rename constants.js
+ run "gulp serve"
