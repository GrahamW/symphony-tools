(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .factory('Admin', Admin);

  /* @ngInject */
  function Admin(constants, Firebase, $firebaseObject, $q) {

    var service = {
      isAdmin: isAdmin
    };

    return service;

    function isAdmin(uid) {
      var ref,
        obj;

      ref = new Firebase(constants.firebaseURL + '/admin/' + uid);
      obj = $firebaseObject(ref);

      // if key is in our array user is an admin

      return $q(function(resolve, reject) {
        ref.once('value', function(snap) {
          if (snap.val() === null) {
            resolve(false);
          } else {
            resolve(true);
          }
        });
      });
    }
  }
})();
