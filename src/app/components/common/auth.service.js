(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .factory('Auth', Auth);

  /* @ngInject */
  function Auth(constants, $firebaseAuth, $q, $rootScope, $state, Admin, Notify) {
    var ref = new Firebase(constants.firebaseURL);

    var service = {
      changePassword: changePassword,
      getAuth: getAuth,
      getAuthObj: getAuthObj,
      login: login,
      logout: logout,
      resetPassword: resetPassword
    };

    return service;

    function changePassword(email, oldPassword, newPassword) {
      var authObj = $firebaseAuth(ref);

      return $q(function(resolve, reject) {
        authObj.$changePassword({
          email: email,
          oldPassword: oldPassword,
          newPassword: newPassword
        }).then(function() {
          resolve();
        }).catch(function(error) {
          reject(error);
        });
      });
    }

    function getAuth() {
      var authData = ref.getAuth();
      return authData;
    }

    function getAuthObj() {
      return $firebaseAuth(ref);
    }

    function login(email, password) {
      var defer = $q.defer();

      ref.authWithPassword({
        email: email,
        password: password
      }, function(error, authData) {
        if (error) {
          defer.reject(error);
          // console.log("Login Failed!", error);
        } else {
          // console.log("Authenticated successfully with payload:", authData);
          defer.resolve(authData);
          $rootScope.currentUser = authData;
          Admin.isAdmin(authData.uid).then(function(isAdmin) {
            $rootScope.isAdmin = isAdmin;
            if (isAdmin) {
              Notify.sucess('admin signin');
            }
          });
          $state.go('app');
        }
      });
      return defer.promise;
    }

    function logout() {
      $rootScope.currentUser = null;
      ref.unauth();
      $state.go('logout');
    }

    function resetPassword(email) {
      var authObj = $firebaseAuth(ref);
      return $q(function(resolve, reject) {
        authObj.$resetPassword({
          email: email
        }).then(function() {
          resolve('Password Reset Email Sent');
        }).catch(function(error) {
          reject(error);
        });
      });
    }

  }
})();
