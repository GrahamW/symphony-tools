(function() {
  'use strict';

  angular.module('symphonyTools')
    .constant('constants', {
      'firebaseURL': 'https://symphonytools.firebaseio.com/'
    });

})();
