// wrapper for the toaster servie so that if style needs to changed
// it can be done in one place

// TODO: notifications share the same timeout, this is probably has to do
// with factories being singletons.  Some investigation needed
(function() {
'use strict';

angular.module('symphonyTools')
  .factory('Notify', function(toaster) {

    var timeout = 5000;

    return {
      sucess: function(msg) {
        toaster.pop('sucess', 'Good News!', msg, timeout);
      },

      error: function(msg) {
        toaster.pop('error', 'Problem!', msg, timeout);
      }
    };
  });
})();
