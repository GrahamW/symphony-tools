angular
  .module('symphonyTools')
  .filter('formatDate', function() {
    return function(input, format) {
      // try to convert to momenet object
      var output;
      output = moment(input);

      // check to see if we have a valid date/time
      if (!output.isValid()) {
        // do nothing
        return input;
      } else {
        // return with formatting
        return output.format(format);
      }
    };
  });
