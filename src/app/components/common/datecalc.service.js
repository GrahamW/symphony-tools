(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .factory('DateCalc', DateCalc);

  DateCalc.$inject = [];

  function DateCalc() {
    // var someValue = '';
    var service = {
      getTimeNow: getTimeNow,
      daysBetweenDates: daysBetweenDates
    };
    return service;


    // calculate the times between two dates.  Expecting either a JS Date object,
    // a string that can be parsed by moment, or a moment object.
    // TODO: should I just return milliseconds and use a filter to go to days?
    function daysBetweenDates (startDate, endDate) {
      startDate = moment(startDate);
      endDate = moment(endDate);

      // we need to add one day here as our end date has no time on it, easier
      // to add a day here then make all endDates
      return moment.duration(endDate.diff(startDate)).add(1, 'd').asDays();
    }

    // this will return a valid time by checking the server time and the client
    // time, if client time is valid it will be used for the rest of the session,
    // otherwise server time will be returned.  If enforeServerTime is not set,
    // and server cannot be reached, client time will be used, if enforeServerTime
    // is set and server cannot be reached, error will be returned.


    function getTimeNow() {

      return moment();

      // TODO: get sever time, something to work on when there is a back-end
      // if client time is already valid, use that

      // else try to get time from server

      // if server time and client time are within 30 seconds,
      // make client time as good and return it

      // if server time
    }
  }
})();
