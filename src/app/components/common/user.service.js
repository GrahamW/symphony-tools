/* jshint devel: true */
(function() {
  'use strict';

  angular.module('symphonyTools')
    .factory('User', function($rootScope, constants, $firebaseObject,
      Notify, Auth, $q) {

      var service = {};

      service.getAllUsers = function() {
        var ref = new Firebase(constants.firebaseURL + '/users/');
        var obj = $firebaseObject(ref);

        return obj.$loaded();
      };

      // get the name for a user by uid.  If a uid is passed, get the name for
      // that uid, if no uid, then get the name for the currently logged in user.
      service.getUserName = function(uid) {
        var ref,
          obj;

        // if uid is not passed in then set it to the curent user's uid
        if (!uid) {
          uid = $rootScope.currentUser.uid;
        }

        return $q(function(resolve, reject) {
          ref = new Firebase(constants.firebaseURL + '/users/' + uid);
          obj = $firebaseObject(ref);

          obj.$loaded(function() {
            resolve(obj.name);
          });
        });
      };

      service.resetPassword = function(email) {
        var refAuth = Auth.getAuthObj();

        refAuth.$resetPassword({
          email: email
        }).then(function() {
          Notify.sucess(email + ' was sent a password reset email');
        }).catch(function(error) {
          Notify.error('Could not send reset email to ' + email);
        });
      };

      service.createUser = function(data) {
        // create a new user then set profile using the new user id

        // we need a ref to the base url for this operation
        var refAuth = Auth.getAuthObj();

        // create a user, this only sets email and password
        refAuth.$createUser({
          email: data.email,
          password: data.password
        }).then(function(userData) {
          Notify.sucess('A user with email address: ' + data.email +
            ' and id: ' + userData.uid + ' was created.');

          // set name and email and active
          service.setName(data.name, userData.uid);
          service.setEmail(data.email, userData.uid);
          service.setActive(true, userData.uid);

          // set admin status
          if (data.isAdmin) {
            service.setAdmin(data.isAdmin, userData.uid);
          }

          // set crew status
          if (data.isCrew) {
            service.setCrew(data.isCrew, userData.uid);
          }
        }).catch(function(error) {
          Notify.error(error.message);
        });
      };

      service.setEmail = function(email, uid) {
        var refsetName = new Firebase(constants.firebaseURL + '/users/' + uid);

        refsetName.child('email').set(email);
      };

      service.setName = function(name, uid) {
        var refsetName = new Firebase(constants.firebaseURL + '/users/' + uid);

        refsetName.child('name').set(name);
      };

      service.setAdmin = function(isAdmin, uid) {
        // admin status cannot be stored in the users endpoint since firebase
        // security rules cannot prevent a user without admin access to add
        // an isAdmin key giving them priliged access.  Therefore, a serperte
        // admin endpoint is requried that is protected.

        // local ref required here
        var refSetAdmin;

        refSetAdmin = new Firebase(constants.firebaseURL + '/admin/');

        // if isAdmin then add the key, if not isAdmin set the key to null
        // which will remove it. No need to check beforehand if the key exists,
        // firebase will sort that out.
        if (isAdmin) {
          refSetAdmin.child(uid).set('placeholder', function(error) {
            if (error) {
              Notify.error('Could not set as Admin');
            } else {
              Notify.sucess('User set as admin.');
            }
          });
        } else {
          refSetAdmin.child(uid).update(null, function(error) {
            if (error) {
              Notify.error('Could not unset Admin');
            } else {
              Notify.sucess('User set as admin.');
            }
          });
        }
      };

      service.setActive = function(isActive, uidToSet) {
        var refSetActive;

        if (uidToSet) {
          refSetActive = new Firebase(constants.firebaseURL + '/users/' + uidToSet);

        } else {
          console.log('Users.setActive - no uid specified');
        }

        if (isActive) {
          refSetActive.child('isActive').set(true);
        } else {
          refSetActive.child('isActive').set(false);
        }
      };

      service.setCrew = function(isCrew, uidToSet) {
        // console.log('isCrew: ' + isCrew + ' uid: ' + uidToSet);
        var refSetCrew;

        if (uidToSet) {
          refSetCrew = new Firebase(constants.firebaseURL + '/users/' + uidToSet);

        } else {
          console.log('Users.setCrew - no uid specified');
        }

        if (isCrew) {
          refSetCrew.child('isCrew').set(true);
        } else {
          refSetCrew.child('isCrew').set(false);
        }
      };

      service.getAllUserNames = function() {
        var refAllUsers,
          objAllUsers;

        refAllUsers = new Firebase(constants.firebaseURL + '/users');
        objAllUsers = $firebaseObject(refAllUsers);

        objAllUsers.$loaded(function() {
          return objAllUsers;
        });
      };

      return service;
    });
})();
