(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .factory('Leave', Leave);

  /* @ngInject */
  function Leave($rootScope, Notify, DateCalc, $firebaseObject, constants, $q, $firebaseArray) {
    // console.log(Notify);
    var someValue = '';
    var ref = new Firebase(constants.firebaseURL + '/users/');
    // var obj = $firebaseObject(ref);

    var service = {
      getAllLeaveReq: getAllLeaveReq,
      daysLeaveAvail: daysLeaveAvail,
      totalDaysOfLeave: totalDaysOfLeave,
      addRequest: addRequest,
      getNumPending: getNumPending,
      changeStatus: changeStatus
    };
    return service;

    // takes in the uid, startdate and enddata of a leave request and then
    // adds it to the database.
    function addRequest(uid, startDate, endDate, status) {
      var obj,
        array,
        tmp;

      // if no status is passed the default on a new request is pending
      if (typeof status === 'undefined') {
        status = 'pending';
      }

      // try to assign input to moment objects, if input is already moment object
      // there is no effect
      startDate = moment(startDate);
      endDate = moment(endDate);

      // strip out only the date and then save that in utc timezone
      tmp = startDate.format('YYYY-MM-DD');
      startDate = moment.tz(tmp, 'utc');
      tmp = endDate.format('YYYY-MM-DD');
      endDate = moment.tz(tmp, 'utc');

      console.log(startDate);

      // get firebase ref and obj
      array = $firebaseArray(ref.child(uid).child('leave').child('requests'));

      // return a promise
      return array.$add({
        startDate: startDate.toISOString(),
        endDate: endDate.toISOString(),
        status: status
      });
    }

    function changeStatus(uid, status, leaveId) {
      var obj,
        index,
        tempRef;

      return $q(function(resolve, reject) {
        tempRef = new Firebase(constants.firebaseURL + '/users/' + uid + '/leave/requests/' + leaveId);

        // console.log(tempRef.val());
        tempRef.update({
          status: status
        }, function(error) {
          if (error) {
            Notify.error('Could not update status:' + error);
            reject();
          } else {
            Notify.sucess('Status updated to ' + status);
            resolve();
          }
        });

      });


    }

    // takes in a uid and gets all leave requests, if no uid then uses uid
    // of logged in user
    function getAllLeaveReq(uid) {
      return $q(function(resolve, reject) {
        var leaveObj;

        leaveObj = $firebaseObject(ref.child(uid).child('leave').child('requests'));

        leaveObj.$loaded().then(function(data) {
          resolve(data);
        });
      });
    }

    // days of leave availbile is the total days accured, minus all approved
    // leave requests.

    // TODO: wrap these calls with $q.all()
    // http://stackoverflow.com/questions/15299850/angularjs-wait-for-multiple-resource-queries-to-complete
    function daysLeaveAvail(uid) {
      var totalLeave;

      return $q(function(resolve, reject) {
        totalDaysOfLeave(uid).then(
          function(data) {
            totalLeave = data;

            getAllLeaveReq(uid).then(
              function(data) {
                angular.forEach(data, function(req) {
                  if (req.status !== 'rejected') {
                    totalLeave = totalLeave - (DateCalc.daysBetweenDates(req.startDate, req.endDate));
                  }
                });
                resolve(totalLeave);
              });
          });
      });
    }

    // determines total days of leave an employee has since their hire date,
    // optional paramter is endDate, if not passed default is now.
    // need to pass in a moment object

    function totalDaysOfLeave(uid, endDate) {
      return $q(function(resolve, reject) {
        var result,
          hireDate,
          leaveObj,
          years,
          deferred;

        // set endDate
        if (typeof endDate === 'undefined') {
          endDate = DateCalc.getTimeNow().startOf('day');
        }

        if (typeof uid === 'undefined') {
          Notify.error('totalDaysofLeave no uid');
          reject('no uid');
          return;
        }
        // get hired date
        leaveObj = $firebaseObject(ref.child(uid).child('leave'));
        leaveObj.$loaded().then(function() {
          hireDate = moment(leaveObj.hireDate);

          // determine difference between start date and leave accural, multiple by
          // leave accrual rate
          years = moment.duration(endDate.diff(hireDate)).asYears();

          // return years of service * days accured per year
          var tmp = years * leaveObj.leaveRate;
          resolve(tmp);
        });
      });
    }

    function getNumPending(uid) {
      var numPending = 0;
      var leaveObj;

      return $q(function(resolve, reject) {
        leaveObj = $firebaseObject(ref.child(uid).child('leave'));
        leaveObj.$loaded().then(function() {
          angular.forEach(leaveObj.requests, function(req) {
            if (req.status === 'pending') {
              numPending += 1;
            }
          });
          resolve(numPending);
        });


      });
    }
  }
})();
