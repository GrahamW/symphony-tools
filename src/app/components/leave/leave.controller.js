(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .controller('LeaveController', LeaveController);

  LeaveController.$inject = ['Leave', 'DateCalc', '$modal', '$rootScope', '$state'];

  function LeaveController(Leave, DateCalc, $modal, $rootScope, $state) {
    var vm = this;

    var uid = $rootScope.currentUser.uid;
    var modalInstance;

    vm.DateCalc = DateCalc;
    vm.pendingTableData = {};
    vm.closedTableData = {};
    Leave.daysLeaveAvail(uid).then(
      function(data) {
        vm.daysLeaveAvail = data;
      }
    );





    vm.openLeaveModel = function() {
      // open a modal
      modalInstance = $modal.open({
        animation: true,
        templateUrl: './app/components/leave/leave-add-modal.html',
        controller: 'LeaveAddModalController as vm'
      });

      // handle modal result
      modalInstance.result.then(function(data) {
        data.startDate = moment(data.startDate);
        data.endDate = moment(data.endDate);
        Leave.addRequest(uid, data.startDate,
          data.endDate).then(function() {
          // TODO: busy indicator
          // reload the page which will re-init the controller updating the table
          if ($state.is('app.leave.add')) {
            $state.go('app.leave');
          } else {
            $state.reload();
          }


        });
      });

    };


    activate();

    function activate() {

      if ($state.is('app.leave.add')) {
        vm.openLeaveModel();
      }

      //TODO: build the leave tables
      Leave.getAllLeaveReq(uid).then(function(reqs) {
        angular.forEach(reqs, function(req, key) {
          if (req.status === 'pending') {
            vm.pendingTableData[key] = {
              startDate: req.startDate,
              endDate: req.endDate,
              status: req.status,
              days: 'TODO: get days'
            };
          } else {
            vm.closedTableData[key] = {
              startDate: req.startDate,
              endDate: req.endDate,
              status: req.status,
              days: 'TODO: get days'
            };
          }
        });
      });



      // handle results from LeaveAdd modal

    }
  }
})();
