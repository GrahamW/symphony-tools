(function() {
    'use strict';

    angular
        .module('symphonyTools')
        .controller('LeaveAddModalController', LeaveAddModalController);

    LeaveAddModalController.$inject = ['DateCalc', '$modalInstance', 'Leave', '$state'];

    function LeaveAddModalController(DateCalc, $modalInstance, Leave, $state) {
        var vm = this;

        vm.leaveReq = {};
        vm.leaveReq.startDate = new Date();
        vm.leaveReq.endDate = new Date();
        vm.DateCalc = DateCalc;


        vm.addOneDay = function(date) {
          date = moment(date);
          return date.add(1, 'day');
        };

        vm.ok = function () {
          // TODO: validate data here
          $modalInstance.close(vm.leaveReq);
        };

        vm.cancel = function() {

          $modalInstance.dismiss('cancel');
          if ($state.is('app.leave.add')) {
            $state.go('app.leave');
          }
        };

        activate();

        function activate() {

        }
    }
})();
