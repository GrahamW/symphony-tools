(function() {
    'use strict';

    angular
        .module('symphonyTools')
        .controller('ForgotPasswordController', ForgotPasswordController);

      ForgotPasswordController.$inject = ['Auth'];

      function ForgotPasswordController(Auth) {
        var vm = this;

        vm.submit = submit;

        activate();

        function activate() {

        }

        function submit () {
          Auth.resetPassword(vm.email)
            .then(function() {
              console.log('reset sent');
            });
        }
    }
})();
