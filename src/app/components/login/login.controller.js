(function() {
  'use strict';

  angular.module('symphonyTools')
    .controller('LoginController', function(Auth) {
      var vm = this;

      vm.submit = function() {
        vm.error = null;
        Auth.login(vm.email, vm.password)
          .then(function() {
            // nothing to do here because Auth service will redirect
            // TODO: controller should do the redirect
          }, function(error) {
            // console.log(error.message);
            vm.error = error.message;
          });
      };
    });
})();
