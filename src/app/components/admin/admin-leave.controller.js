(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .controller('AdminLeaveController', AdminLeaveController);

  AdminLeaveController.$inject = ['$rootScope', 'Leave', 'User'];

  function AdminLeaveController($rootScope, Leave, User) {
    var vm = this;

    vm.tableData = {};

    vm.allUsers = User.getAllUsers().then(
      function(data) {
        angular.forEach(data, function(value, key) {
          vm.tableData[key] = {
            name: value.name,
            hireDate: value.leave.hireDate,
            leaveRate: value.leave.leaveRate

          };
          Leave.totalDaysOfLeave(key)
            .then(function(data) {
              vm.tableData[key].totalLeaveAccured = data;
            });
          Leave.daysLeaveAvail(key)
            .then(function(data) {
              vm.tableData[key].daysLeaveAvail = data;
            });
          Leave.getNumPending(key).then(
            function(data) {
              vm.tableData[key].pendingReq = data;
            }
          );
        });
      }
    );

    // create an object that holds user names, hire date, leave accured
    Leave.totalDaysOfLeave($rootScope.currentUser.uid)
      .then(function(data) {
        vm.totalLeave = data;
    });
  }
})();
