(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .controller('AdminLeaveDetailController', AdminLeaveDetailController);

  AdminLeaveDetailController.$inject = ['$stateParams', 'Leave', 'DateCalc',
    'User', '$state'
  ];

  function AdminLeaveDetailController($stateParams, Leave, DateCalc, User,
    $state) {

    var vm = this;


    vm.uid = $stateParams.uid;
    vm.pendingTableData = {};
    vm.closedTableData = {};
    vm.DateCalc = DateCalc;
    vm.changeLeaveStatus = changeLeaveStatus;


    activate();

    function activate() {
      getName(vm.uid);
      buildTables();
    }

    function getName(uid) {
      User.getUserName(uid).then(
        function(name) {
          vm.name = name;
        }
      );
    }

    function changeLeaveStatus(uid, status, leaveId) {
      Leave.changeStatus(uid, status, leaveId).then(
        function() {
          $state.reload();
        }
      );
    }

    function buildTables() {
      Leave.getAllLeaveReq(vm.uid).then(function(reqs) {
        angular.forEach(reqs, function(req, key) {
          if (req.status === 'pending') {
            vm.pendingTableData[key] = {
              startDate: req.startDate,
              endDate: req.endDate,
              status: req.status,
              days: 'TODO: get days',
              key: key
            };
          } else {
            vm.closedTableData[key] = {
              startDate: req.startDate,
              endDate: req.endDate,
              status: req.status,
              days: 'TODO: get days',
              key: key
            };
          }
        });
      });
    }
  }
})();
