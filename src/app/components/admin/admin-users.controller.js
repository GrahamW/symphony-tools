(function() {
'use strict';

angular.module('symphonyTools')
  .controller('AdminUsersController', function(User) {
    var vm = this;

    vm.allUsers = {};

    User.getAllUsers().then(function(data) {
        vm.allUsers = data;
        // console.log(vm.allUsers);
    }, function(error) {
      console.log('could not load users list');
    });

    vm.submitCreateUser = function () {
      User.createUser(vm.createUser);

      vm.createUser = {};
    };

    vm.resetPassword = function(email) {
      User.resetPassword(email);
    };

    vm.toggleIsCrew = function(isCrew, uid) {
      // console.log(isCrew + uid);
      User.setCrew(isCrew, uid);
    };

    vm.toggleIsActive = function(isActive, uid) {
      User.setActive(isActive, uid);
    };
  });
})();
