(function() {
'use strict';

angular.module('symphonyTools')
  .controller('DashboardController', function(User) {
    var vm = this;

    User.getUserName().then(function(data) {
      vm.name = data;
    });
  });
})();
