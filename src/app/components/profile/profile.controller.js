(function() {
  'use strict';

  angular
    .module('symphonyTools')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['Auth', 'Notify'];

  function ProfileController(Auth, Notify) {
    var vm = this;

    vm.changePassword = changePassword;

    activate();

    function activate() {

    }

    function changePassword() {
      // TODO: get email for current user
      var email = 'graham@builtprecise.io';

      // call Auth change password
      Auth.changePassword(email, vm.passwordReset.currentPassword, vm.passwordReset.newPassword0)
        .then(function() {
          Notify.sucess('Password Changed');
          vm.passwordReset = {};
        }, function(error) {
          Notify.error('Problem:' + error);
          console.error(error);
        });
    }
  }
})();
