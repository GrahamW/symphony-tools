(function() {
'use strict';

angular.module('symphonyTools')
  .controller('NavbarController', function ($scope, $state, Auth, $rootScope) {
    var vm = this;

    // so that nav menu loads collapsed by default
    vm.isCollapsed = true;
    vm.state = $state;
    console.log($rootScope);
    vm.isAdmin = $rootScope.isAdmin;

    vm.reqLogout = function() {
      Auth.logout();
    };
  });
})();
