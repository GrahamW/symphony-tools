// declare the module.  This app will only have one module as pieces won't
// be reused elsewhere.
(function() {
  'use strict';

  angular.module('symphonyTools', [
    'ngMessages',
    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'uiRouterStyles',
    'firebase',
    'toaster'
  ]);
})();
