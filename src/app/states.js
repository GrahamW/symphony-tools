(function() {
  'use strict';

  angular.module('symphonyTools')
    .run(function($rootScope, $state, Auth, Admin) {

      $rootScope.$on('$stateChangeStart', function(event, toState) {
        var requireLogin = toState.data.requireLogin;
        var requireAdmin = toState.data.requireAdmin;

        // check if requireLogin is true but user is not set
        if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
          // if user if not set, check with Auth first to see if previous session
          // exists.
          var authInfo = Auth.getAuth();

          if (!authInfo) {
            // invalid user so send to login state
            event.preventDefault();
            $state.go('login');
          } else {
            // auth remembers session so set the currentUser
            $rootScope.currentUser = authInfo;
          }
        }

        // check to see if user is an admin, if not redirect to 'app' state
        if (requireAdmin) {
          // console.log($rootScope.currentUser.uid);
          Admin.isAdmin($rootScope.currentUser.uid).then(
            function(isAdmin) {
              if (!isAdmin) {
                event.preventDefault();
                $state.go('app');
              }
            }
          );
        }
      });
    });

  angular.module('symphonyTools')
    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('login', {
          url: '/login',
          views: {
            'content': {
              templateUrl: '/app/components/login/login.html',
              controller: 'LoginController as vm'
            },
            'footer': {
              templateUrl: '/app/components/footer/footer.html'
            }
          },
          data: {
            requireLogin: false,
            css: '/app/components/login/login.css'
          }
        })

      // logout state is the same as login for now. Might have a logout
      // template later TODO: logout template?
      .state('logout', {
        url: '/login',
        views: {
          'content': {
            templateUrl: '/app/components/login/login.html',
            controller: 'LoginController as vm'
          },
          'footer': {
            templateUrl: '/app/components/footer/footer.html'
          }
        },
        data: {
          requireLogin: false,
          css: './app/components/login/login.css'
        }
      })

      .state('forgotpassword', {
        url: '/forgotpassword',
        views: {
          'content': {
            templateUrl: '/app/components/login/forgotpassword.html',
            controller: 'ForgotPasswordController as vm'
          },
          'footer': {
            templateUrl: '/app/components/footer/footer.html'
          }
        },
        data: {
          requireLogin: false,
          css: './app/components/login/login.css'
        }
      })

      // below here routes require user to be logged in
      .state('app', {
        url: '/',
        views: {
          'nav': {
            templateUrl: '/app/components/navbar/navbar.html',
            controller: 'NavbarController as vm'
          },
          // default view is dashboard
          'content': {
            templateUrl: '/app/components/dashboard/dashboard.html',
            controller: 'DashboardController as vm'
          },
          'footer': {
            templateUrl: '/app/components/footer/footer.html'
          }
        },
        data: {
          // will apply to all app child views
          requireLogin: true
        }
      })

      .state('app.profile', {
        url: 'profile',
        views: {
          'content@': {
            templateUrl: 'app/components/profile/profile.html',
            controller: 'ProfileController as vm'
          }
        }
      })

      .state('app.leave', {
        url: 'leave',
        views: {
          'content@': {
            templateUrl: '/app/components/leave/leave.html',
            controller: 'LeaveController as vm'
          }
        }
      })

      .state('app.leave.add', {
        url: '/add',
        views: {
          'content@': {
            templateUrl: '/app/components/leave/leave.html',
            controller: 'LeaveController as vm'
          }
        }
      })

      // below here routes requrie admin level access
      // TODO: restrict access to admin
      .state('app.admin', {
        url: 'admin',
        views: {
          'content@': {
            templateUrl: '/app/components/admin/admin.html'
          }
        },
        data: {
          requireAdmin: true
        }
      })

      .state('app.admin.leave', {
        url: '/leave',
        views: {
          'content@': {
            templateUrl: '/app/components/admin/admin-leave.html',
            controller: 'AdminLeaveController as vm'
          }
        }
      })

      .state('app.admin.leave.detail', {
        url: '/:uid',
        views: {
          'content@': {
            templateUrl: '/app/components/admin/admin-leave-detail.html',
            controller: 'AdminLeaveDetailController as vm'
          }
        }
      })

      .state('app.admin.users', {
        url: '/users',
        views: {
          'content@': {
            templateUrl: 'app/components/admin/admin-users.html',
            controller: 'AdminUsersController as vm'
          }
        }
      });

      $urlRouterProvider.otherwise('/');
    });
})();
